---
title: 外包断层问题
tags: []
date: 2011-02-14 11:00:00
---

一年一度的通信论坛如期而至，三年来，每期我都游走中间，只是目标有所不同．第一年短实习，即便抱着只要有就成的态度，也碰的焦头烂额，第二年长实习虽没有崭获，但信心正在建立．第三年，冷静出击，只为试试职场大海，水有多深．和法国农业信用银行的一段讨论，引出了这个外包断层问题．<!--more-->

受最后一个实习的影响，我希望能以一个开发者的姿态开始自己的职业生涯．走到他们的台前，不慌不忙的说明了来意．话音刚落，人家就反问一句，你确实只喜欢做开发工作吗？

心想，难道刚表达完心愿，就直接没戏了？不过还是要坚持自己的爱好．我说是的，我想以这个作为开始，也许若干年后可以从事其它的工作．

人家很委婉的说，现在的大部分新的开发工作都外包到遥远的东方了．所以公司的招聘政策趋向于招收具有丰富经验的技术人员来负责项目的策划和验收．因为毕竟无论中间的过程怎么外包，一是要先表达需求，再是验收外包成果，保证产品质量．并且做验收的必须具有熟练的职业技能，要不无法给别人的工作做定论．

问题就出于此，一旦外包成为趋势．在当地，能为开发者提供的就业机会就会越来越受到限制．而市场上对了解当地行情的高级技术人员的需求却有增无减．不要忘记，一切高级的物种都是经历初级，中级慢慢积累演变而来的．不培养年经的初级工程师，何来经验丰富的专家．

问题虽然存在，但我们却恰恰能从问题中找到启示．趋势在此，但并不证明所有的公司都外包殆尽．在困境中寻找锻炼机会，汲取工作经验，就有机会成为日后为数不多的高级技术工程师中的一员．

此外，对行业的了解也显得愈发的重要．即便是技术人员，能做到上可以谈需求，下可以读代码，才能游刃有余．