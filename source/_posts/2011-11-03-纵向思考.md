---
title: 纵向思考
tags: []
date: 2011-11-03 11:00:00
---

小猫钓鱼，还想着蝴蝶，三心二意，结果，两手空空，终无所获。拿这个家喻户晓的故事当作警示，却常常发现，自己又何尝不是一只三心二意的猫。

初秋的时候，在同事的推荐下，决定读一下佐拉的萌芽（germinal），可迫于书中晦涩的法语，当阅读变成一种训练的时候，转向了广受好评的明朝那些事。这次体验是轻松加愉快，但随着新的想法不断冒出，那些事还没读完七分之一，又开篇尝试，被称为企业家必读之书，艾因兰德的名作，源头。除此之外，平日里，还胡乱的看了一些技术方面的著作。

说的矫情点，这叫培养发散性思维。其实呢，这是典型的浅尝辄止，不求甚解，成不了大业。二十一世纪的今天，互联网的出现和成熟，使信息的传播速度不知道比明朝快了多少万倍。但不可否认的是，它是一把双刃剑，同时加速了注意力的转移速度。舆论的误导诱惑也罢，自身的浮躁短视也好，人们变得越来越耐不住寂寞，越来越沉不下来气，纵向思考。

所谓纵向思考，就是集中精力深入的做好一件事情，善始善终。我想，大凡美好的事物，往往隐藏的很深。没有一颗纵向思考，勇于探索，耐得住寂寞的心，是很难发现它们的。这也许正是它们之所以美丽的原因。