---
title: 作为研发经理，你对产品经理知多少
date: 2021-01-09 22:38:31
tags:
- 创新
- 研发经理
- 产品经理
---
研发经理，有的抓产品研发，有的扛系统运维。如果你负责研发，那么产品经理将是你的关键合作伙伴。你们工作虽然有交集，却隔行如隔山，扮演的角色不同，思维方式就不同，所需的技能也不同。能够充分了解你的产品经理，与他优势互补，是你们合作构造成功产品的助推器。

产品经理是你的合作伙伴，我们可以从如何了解一个合作伙伴开始，然后再把这种办法具体应用到产品经理上。了解一个合作伙伴，我有一个框架可以用，具体分三步走。

首先是认清你们的共同目标，这是最为关键的一步。因为只有认清你们要往哪里走，才能力往一处使。倘若大家对目标的理解不同，所做的一切都将成为对方前往目标的阻力。

其次是搞明白达成此目标，不同的合作者，如何贡献他们的能力。这也是团队合作的魅力，每个人的优势都不一样，一个人很难十八般武艺，样样精通，只有拧在一起，才能完成个人无法完成的事。

最后，通过分析不同合作者，能力的共同点和差异点，来更好的认识你的合作者。共同点一般比较好理解，因为你们有共同的语言，这里要想充分的理解一个人，要把重点放在差异点上。

框架勾勒出来了，现在把问题具体到研发经理和产品经理上。你们的共同目标是什么？一般情况下，你们是想开发出一款用户爱用，愿意花钱买，能解决他们问题的产品。这个产品小到可以是一个闹钟应用，大到是一个搜索引擎。可不管是什么样的产品，它都得必须能解决一个实际问题。团队中，不论是程序员，测试员，还是产品经理，没有一个人乐意去做一个没人用的东西。

确定目标是最关键的一步，也是最难的一步。有一个小技巧，可以帮助你们找到共同的目标，就是多追问几下，追问的过程，其实一个追根溯源的过程，看似不相关的东西，往根上挖一挖，就有了联系。这个技巧有一个陷阱，就是不要挖的太深，太深了就开始虚了，别到最后，发现你们的目标是改变世界。

目标清晰了，开发出一个款解决用户痛点的产品。这需要什么样的能力？我们可以概括为发现问题，选择问题，解决问题的能力。除此之外，还包括敏捷循环使用这些能力的迭代能力。

首先要发现问题，得有人去和客户打交道，摸清出他们的需求，搞明白什么东西能给他们带来价值，以至于他们愿意去花钱买你的产品和服务。然而很多情况下，用户的需求不是单一的，而是复杂的，系统性的，有时还会受到外部因素的影响，如何从众多需求中找到最关键的？这就是第二步，选择问题，做出好的选择需要多方面的权衡，这包括竞争对手走到了哪一步，公司需要生存下来是否需要做出短期的牺牲来赢取更多的时间，某一需求的投资回报率是多少有没有快速收益的方案。经过多方权衡，需要解决的问题定下来之后。第三种能力就该派上用场，这就是解决问题的能力。通常办法是团队开发完整解决方案。验证能力强的人，会争取把这个过程缩到最短，他会找到最短路径。这一套走下来，有不得已的权衡，有为了省时而走的捷径，并非解决了所有问题，所以还要很多轮的迭代。

目标清晰，达成目标所需要的能力列举的明明白白，接下来就看这些能力都有谁贡献。作为研发经理，我们最擅长的解决问题的能力。产品经理，最擅长的则是敏锐的发现问题的能力，以及在商业，竞争对手，内部资源各种掣肘限制下的选择问题的能力。这里所说的能力，是高度概括出来的，还可以细分。比如说发现问题的能力，这要求拥有此能力的人，能够非常有效的和人沟通，通过交流，洞察并归纳出问题的本质。选择问题的能力，必定有大局观的支撑，如果只能片面的看到一方的限制因素而忽略了其他，则无法做出好的决定。

在这个框架之下，我们可以总结一下一个优秀的产品经理应该有哪些可以和你互补的优势？他深刻理解企业的商业目的，并且能通过解决客户的问题来达成此目的。他有高效的沟通能力，同理心，观察能力，能准确抓住用户的痛点。他还有足够大的大局观，能看到客户，竞争对手，企业内部运营给他框定的各种限制。他还要有影响力，能够让人看到他勾勒的愿景，并愿意为此去奋斗。

以上的思考有受到Melissa Perri的Escaping the Build Trap: How Effective Product Management Creates Real Value的启发，强烈推荐给对产品管理有浓厚兴趣的研发经理和产品经理们。