---
title: 包容的回答者
date: 2021-05-12 12:45:55
tags:
- 学习
- 正念
- 批判性思维
---
当我第一次思考蠢问题的时候，我关注的是蠢问题的定义：什么是蠢问题？。后来一位读者反馈到，“给蠢问题下定义没什么意义。应该放下我执，想问就敢问，得到答案比面子重要”。我这才意识到我的做法是非常片面的。尤其是回想起【改变问题，改变人生】书中说到的学习者心态以后，我才恍然大悟，**哪里有什么蠢问题，只有因为评判而错失的学习机会**。当我们被冒犯而把问题当做蠢问题的时候，就进入了批判者模式，走上的是互相指责、伤害的羊肠小路。而怀着包容的心态，一个学习者，走的是一条通往理解、进步的康庄大道。

作一个包容的回答者，特别是对于决定要带团队的人，至关重要。因为我们常常面对的是新人的提问，我们在回答问题的时候，很自然的就会出现地位上的差异。在这种差异下，包容的回答者，既可以培养他们的自信，又能保证他们的发展。

# 所谓的蠢问题

下面是几个容易被当做蠢问题的问题，让我们看一下批判者和学习者分别是如何应对的。

## 没经思考的问题

有一次，一个新来的同事问，“如何在Github里创建一个仓库？” 

作为评判者，特别是一个日程满当当的评判者，可能会被惹恼，心想，“这么显而易见的东西，难道你不知道答案吗，我都怀疑你是怎么通过的面试”。如果我们因此说这个问题太蠢，自己生气不说，提问者的自尊也会受到伤害，他可能觉得自己太笨，能力太弱。

换成学习者，我们心里可能会这么想。这个答案对我很明显，可他为什么没明白呢？是我的经验比他多很多吗？还是这个人没有思考？还是他真正想问的是另外一个问题？

与其说这个问题蠢，不如反问一下，这个问题你是怎么思考的？你真正的需求是什么？后来才明白，他真正关心的是创建仓库的注意事项。幸好没有给他一棒子打死。可别说，我们创建仓库还真不是点个按钮那么容易，为了保证仓库的私密性和明确的责任团队，仓库的创建被自动化了，申请人需要创建一个PR，提供必要的信息，通过审核后，才可以得到想要的仓库。

## 问了又问的问题

又有一次，是一个初涉职场的年轻毕业生，“Tech lead和技术经理有什么差别？”这个问题他在不到两周的时间内问了我两次。

当一个问题被问了第二遍的时候。我们的批判者思维特别容易被激活，这是一个非常让人恼火的事情。难道我第一遍讲的事情都被当成啤酒喝了吗。

而深呼一口气，转换成一个学习者，我们会想，这个问题我第一次已经给他讲过一遍了，是他忘了吗？还是我第一次没有讲清楚？还是问题有了新的进展，他有了新的想法？这时候我们可以说，这个问题我们上次讨论过一次，可能是我当时没有讲清楚，你是有了新的想法了吗？这时候你很尊重别人，他很有可能说出新的想法，也可能是问题太复杂，真的需要解释好几遍才行。

上面这个问题，后来我解释了第二遍。再后来，各小组经理开会的时候发现，很多人都有这个疑问。被解释了好几遍，并且真正和Tech Lead以及技术经理共事后，人们才会真正明白其中的区别。由此看来，重要的事不仅要说三遍，而且还得通过实践的检验。

## 问错人的问题

还有一次，一个产品经理，问我们一个后端工程师，“如何激活一个功能开关让她测一个新功能？”

我们对所问的事情一窍不通。作为批判者，我们也可能被惹恼，说你问问题之前，为什么不了解一下我呢，这个问题和我一点关系都没有啊。拜托你能不能在问之前，先了解一下谁是干什么的。

而作为学习者，我们会心想。是有人给他推荐错了人吗。还是说我们公司里缺少对团队职能的介绍，所以他才拿了这个问题问了错的人。我们可以反问他，对不起，我在这个问题帮不了你，有可能是有人给你提供了错误的信息，我不是这方面的专家，你是怎么找到我的？这时候，这个人会把他如何来的讲清楚，我们就有了一个提升团队职能介绍的机会，可以帮助更多新人找到对的人问问题。

回到这个无辜的产品经理上，人家注重的是功能，怎么能分清那么多的前端，后端，全栈工程师的职责。她的问题恰恰可以督促团队澄清每个人的责任范围。

## 不清楚的问题

另外一次，是一个家政阿姨，我本来问她善长做什么菜，她问我，“你家最近的地铁站是哪里？”

听完了她的提问，我们都不知道她在问什么？或者知道她在问什么，却不知道她为什么这样问。作为批判者，我们会说，我根本不知道你再问什么，你能想好了再问吗。

而作为学习者，我们会想，她的问题不清楚啊，我不知道她在问什么，是不是他来的太急了，还是她之后还有其他的问题。她真正的目的是什么呢？这时候，我们可以反问，“对不起，我没搞清楚这个问题，你能具体的解释一下你的目的吗？“。这样可以引导提问者找出她所面临的真正的挑战。

这位阿姨，问位置，其实想优化自己的行程，在她看来，如果离的太远，我做的菜再好也没用啊，我犯的起从城东头跑到西头做一个红烧茄子吗。

# 学习者心态

通过上面的几个例子，我们可以看出，要培养学习者心态，一共可以分为三步

- 接纳问题：不管问题有多么的让人恼火，我们都要避免心智被本能控制。当你的思维被评判者绑架的时候，要像邢捕头那样，想想为什么，世界这么美妙，而我如此暴躁。意识到批判思维，有助于把学习者找回来。说到接纳，我想起了非暴力沟通让我学会了接纳，有异曲同工之妙。再延伸一下，明白正念概念的朋友，会发现这里用的正是此技术。
- 分析问题：接纳之后，我们就创造了深入认识所提问题的机会。造成这种局面的原因有可能是什么呢？是提问者的原因，还是回答者自身的原因？这时候我们可以用一种尊重别人的方式，问对方几个问题，确认之前的种种猜测，搞清楚问题的来龙去脉。
- 解决问题：前后脉络都清晰的时候，我们知道了对方真正的目的，根据提问方，我们可以选择回答，也可以选择通过提问的方式去引导。当然了，如果是老板在问问题，我建议最好不要去引导。

我相信，这种心存公允，谦虚好奇的处理方式，对提问者和回答者都大有益处，因为它会在团队里，催生尊重、信任，鼓励大家共同学习、进取。

这里我们对回答者提了很多要求，他们付出了很多的耐心，给予了很多的包容，去营造双赢的局面，作为提问者，我们如何做能对得起这份耐心和包容呢？更何况人非圣贤，包容也是有限度的，我想我们很有必要去探讨一下如何做一个自律的提问者，特别是我们在现实生活中会在这两个身份之间来回转换，真正的可持续长期健康发展，来自于包容的回答者和自律的提问者。