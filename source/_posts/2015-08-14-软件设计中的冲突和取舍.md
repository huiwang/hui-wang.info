---
title: 软件设计中的冲突和取舍
tags: [软件设计]
date: 2015-08-14 11:00:00
---

这是看完[Aeron](https://github.com/real-logic/Aeron/wiki/Design-Principles)的设计原则之后的感触。关键的不是看他们设计原则本身是在讲什么，而是这些原则如何指导，对各种冲突的权衡与取舍。

不仅仅是在软件设计中，即便是生活里，冲突随处可见。拿出一个解决办法，满意了小明，却顾不上小红。另一个方案，取悦了小红，却委屈了小明。何去何从，我们需要取舍。

取舍要有原则，可原则怎么定？像Aeron里做的那样，把你考虑到系统的各个方面按他们的重要性排序。如果说小明比小红重要，两个有冲突的解决方案的取舍，就应该从小明的角度来定。

软件的架构，最核心的步骤，就是定义这些取舍的原则，原则清晰了，系统的设计也就差不多了。
