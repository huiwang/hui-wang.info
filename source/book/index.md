---
title: 读书
date: 2017-12-12 20:41:12
---
{% note success %}
读书帮我释放着对未知世界的热情
{% endnote %}

# 工程师进阶
- 数学之美
[吴军](https://sites.google.com/site/junwu02/%E4%B8%AD%E6%96%87%E7%89%88%E8%AF%B7%E6%8C%89%E8%BF%99%E9%87%8C)博士，著名自然语言处理专家。本书用简单易懂的语言讲述了数学在计算机中的应用。我很后悔没有更早的接触到这本书，要不然就会投入更多的时间学习数学，如果你还在用[凑的方式](https://hui-wang.info/2017/11/25/%E6%95%B0%E5%AD%A6%E4%B9%8B%E7%BE%8E%EF%BC%9A%E4%B8%8D%E8%83%BD%E5%86%8D%E5%87%91%E4%BA%86/)解决问题，那么看看数学之美吧！

- 赤裸裸的统计学 - Naked Statistics
Charles Wheelan，美国经济学家。如果你是一个门外汉，但又很想了解统计学，估计你能把这本书读完，因为里面没有一个数学公式。统计学，可以帮助你擦亮眼睛，看清数据背后的真实情况。特别是在大数据的时代背景下，统计学将更加广泛的渗透到每个人的日常生活中。

- 算法 - Algorithms
Robert Sedgewick，40后，美国人，在普林斯顿任教，给Donald Knuth当过学生。这本书是我看过的最好的算法书，搭配上他在Coursera上的[公开课](https://www.coursera.org/learn/algorithms-part1)，你不需要再看其他类似的书了。在上他公开课的时候，他偶尔会咳嗽，当时看着特别不忍心，一头白发，还在孜孜不倦的给人传授知识，致敬！通过上他的课，我第一次在算法竞赛中脱颖而出，[积累的效果显现了
](https://hui-wang.info/2016/04/05/%E7%A7%AF%E7%B4%AF%E7%9A%84%E6%95%88%E6%9E%9C%E6%98%BE%E7%8E%B0%E4%BA%86/)。

- 程序员面试金典 - Cracking the Coding Interview - Gayle Laakmann McDowell
面试官都在读的书，因为他们面试也需要出题，练习这本书，你就做到了有的放矢。我给书中的练习加上了单元测试，开源代码在[这里](https://github.com/huiwang/cracking-the-coding-interview)，好用请给我发个星星。

- Algorithms to Live By - 赖以生存的算法 
Brian Christian, Tom Griffiths把最核心的几个算法用生活中的点滴讲了出来。我有感而发，写了几篇体会：[过拟合
](https://hui-wang.info/2018/01/14/%E8%B5%96%E4%BB%A5%E7%94%9F%E5%AD%98%E7%9A%84%E7%AE%97%E6%B3%95%EF%BC%9A%E8%BF%87%E6%8B%9F%E5%90%88/), [缓存](https://hui-wang.info/2018/01/07/%E8%B5%96%E4%BB%A5%E7%94%9F%E5%AD%98%E7%9A%84%E7%AE%97%E6%B3%95%EF%BC%9A%E7%BC%93%E5%AD%98/)。

- [Operating Systems: Three Easy Pieces](https://pages.cs.wisc.edu/~remzi/OSTEP/)
人家夫妻双双把家还，Arpaci-Dusseau夫妇写了一本生动形象的操作系统的书，趣味性非常强，利于消化吸收。我也写了一篇读后感[操作系统对CPU的控制权](https://hui-wang.info/2018/03/25/%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F%E5%AF%B9CPU%E7%9A%84%E6%8E%A7%E5%88%B6%E6%9D%83/)。

- 设计数据密集型应用 - Design Data Intensive Application
[Martin Kleppmann](https://martin.kleppmann.com/)，原Linkedin工程师，成功的创办过两家企业，如今在母校剑桥大学，从事分布式系统的研究工作。2017年，数密一经发布，便成为了分布式系统的畅销书。这是我读过的最系统，最全面，最与时俱进的分布式系统书籍：从基础的数据结构（HashMap，B-Tree，LSM-Tree），到数据库及数据模型，再到分布式系统中的一致性和共识算法，一直到数据处理系统中的流处理，这本书覆盖了数密系统中的方方面面。另外，这本书还引用了[大量的论文](https://github.com/ept/ddia-references)，堪称分布式系统中的知识宝藏。

- 代码整洁之道 - Clean Code
[Robert C. Martin](https://sites.google.com/site/unclebobconsultingllc/)，50后，人称Bob大叔。如果你叫不上他的名字，可你也许听说过SOLID设计原则，或者听说过[敏捷软件开发宣言](http://agilemanifesto.org/iso/zhchs/manifesto.html)。他的SOLID原则和这本书，影响了无数的程序员的设计思想，代码风格。我几乎想不到，还能有谁能对整个软件行业产生这么深远而广泛的影响。除了这本书，你也许会喜欢他谈[编程的未来](https://www.youtube.com/watch?v=ecIWPzGEbFc)。

- 重构 - Refactoring
[Martin Fowler](https://martinfowler.com/)，Martin是一个你认识了就会觉得相识恨晚的人，你会发现你所了解的很多行业里的名词都是从Martin那里来的。我在2011年的时候专门总结过[Martin对我带来的改变](https://hui-wang.info/2011/11/14/Martin-Fowler/)。懂得了重构，就懂得了与时俱进，就懂得了伟大的产品是通过不断的打磨得来的。在所有的重构工具里，[Intellij](https://www.jetbrains.com/)达到了登峰造极的境界。

# 管理和领导力
- 非暴力沟通 - Nonviolent Communication
Marhsall B. Rosenberg的这本书是帮助我提升沟通能力最大的一本书。没接触这本书的时候，我明白了[不要和人争吵](https://hui-wang.info/2016/01/21/%E4%B8%8D%E8%A6%81%E5%92%8C%E4%BA%BA%E4%BA%89%E5%90%B5/)。后来自己更加成熟，才发现非暴力沟通[让我学会了接纳](https://hui-wang.info/2021/02/16/%E9%9D%9E%E6%9A%B4%E5%8A%9B%E6%B2%9F%E9%80%9A%E8%AE%A9%E6%88%91%E5%AD%A6%E4%BC%9A%E4%BA%86%E6%8E%A5%E7%BA%B3/)，学会了真诚的[表扬下属](https://hui-wang.info/2020/12/26/%E5%A6%82%E4%BD%95%E8%A1%A8%E6%89%AC%E4%B8%8B%E5%B1%9E/)，学会了用爱帮助别人。 

- 管理、成就、生活
从[程序员转了管理之后](https://hui-wang.info/2020/10/06/%E7%A8%8B%E5%BA%8F%E5%91%98%E4%B8%BA%E4%BB%80%E4%B9%88%E8%BD%AC%E5%9E%8B%E7%AE%A1%E7%90%86%EF%BC%9F/)，我一部分的管理知识来自试错的经验积累，一部分是领导的言传身教。欧洲管理学大师[马利克](https://wiki.mbalib.com/wiki/%E5%BC%97%E9%9B%B7%E5%BE%B7%E8%92%99%E5%BE%B7%C2%B7%E9%A9%AC%E5%88%A9%E5%85%8B)的这本书帮我认识到管理可以作为一种职业，通过其四大基本要素：任务，工具，原则和职责，[进行系统化的学习](https://hui-wang.info/2020/06/07/%E5%A6%82%E4%BD%95%E7%B3%BB%E7%BB%9F%E5%AD%A6%E4%B9%A0%E7%AE%A1%E7%90%86%E5%AD%A6%EF%BC%9F/)。

- 影响力 - Influence: Science and Practice
作为程序员，我曾经困惑过，怎么说服领导采用我的解决方案？带着这个问题，我读了誉满全球的说服大师，西奥迪尼，写于1984年的这一经典著作。与其说是我学会了影响别人，倒不如说如何不受他人影响。这里我写了对我[触动最大](https://hui-wang.info/2018/02/04/%E5%BD%B1%E5%93%8D%E5%8A%9B/)的几个实例。

- 金字塔原理 - The Pyramid Principle
沟通能力的提升，要软硬兼施，如果说非暴力沟通是软，那么金字塔原理就是硬。要想和别人有效的沟通，最重要的是把自己的想法清晰的表达出来。Barbara Minto的金字塔原理，帮助你[有逻辑的组织思维](https://hui-wang.info/2020/04/26/%E9%87%91%E5%AD%97%E5%A1%94%E5%8E%9F%E7%90%86/)，更加容易的让人准确的接收到你的信息。

- 教练的习惯 - The Coaching Habit
Michael Stanier介绍的七个问题帮助管理者[做好团队的教练](https://hui-wang.info/2021/03/31/%E5%A6%82%E4%BD%95%E5%81%9A%E5%A5%BD%E5%9B%A2%E9%98%9F%E6%95%99%E7%BB%83%EF%BC%9F/)。让管理者和团队成员进行更有效的对话。一方面帮助管理者更省心，更高效的带领团队，另一方面帮助团队成员更有深度的思考，实现自身的成长。

- 改变提问，改变人生 - Change Your Questions, Change Your Life: 12 Powerful Tools for Leadership, Coaching, and Life
Marilee Adams故事里的主人翁，一直都是一个知道先生，而当他需要带团队的时候，发现知道先生行不通了。后来经过一系列的培训，学会了如何从评判者转换到学习者，进而提升了团队领导力，也使得家庭生活变得更加和谐。这种转换，是一种意识上的觉醒，是学会了理性的思考，摒弃了无意识的本能反应。